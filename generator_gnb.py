import random
from itertools import combinations
from itertools import cycle
import os
import shutil
import glob
import numpy as np
import time


def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)

# delete all txt files in the folder
points = np.array([[10, 2950 - 1697.5],
          [10, 2950 - 947.5],
          [10, 2950 - 10],
          [1447.5, 2950 - 10],
          [3260, 2950 - 10],
          [4010, 2950 - 10],
          [4010, 2950 - 385],
          [4010, 2950 - 822.5],
          [4010, 2950 - 1385],
          [4010, 2950 - 1947.5],
          [4010, 2950 - 2385],
          [4010, 2950 - 2885],
          [3260, 2950 - 510],
          [2822.5, 2950 - 510],
          [2322.5, 2950 - 510],
          [3260, 2950 - 760],
          [3260, 2950 - 1010],
          [3260, 2950 - 1385]])

from sklearn.cluster import KMeans

for gnbs in range(2,16):
    kmeans = KMeans(n_clusters=gnbs, random_state=0, n_init="auto").fit(points)

    clusters = kmeans.cluster_centers_

    print(clusters)



    t1 = """
import inet.networklayer.configurator.ipv4.Ipv4NetworkConfigurator;
import inet.networklayer.ipv4.RoutingTableRecorder;
import inet.node.ethernet.Eth10G;
import inet.node.inet.Router;
import inet.node.inet.StandardHost;
import simu5g.common.binder.Binder;
import simu5g.common.carrierAggregation.CarrierAggregation;
import simu5g.nodes.Upf;
import simu5g.nodes.NR.gNodeB;
import simu5g.nodes.NR.NRUe;
import simu5g.nodes.backgroundCell.BackgroundCell;
import simu5g.world.radio.LteChannelControl;

network MultiCell_Standalone
{
    parameters:
        int numUe = default(1);
        int numBgCells = default(0);
        @display("i=block/network2;bgb=1088.032,678.896;bgi=background/pisa");
    submodules:
        channelControl: LteChannelControl {
            @display("p=50,25;is=s");
        }
        routingRecorder: RoutingTableRecorder {
            @display("p=50,75;is=s");
        }
        configurator: Ipv4NetworkConfigurator {
            @display("p=50,125");
        }
        binder: Binder {
            @display("p=50,175;is=s");
        }
        carrierAggregation: CarrierAggregation {
            @display("p=50.993748,258.7;is=s");
        }
        server: StandardHost {
            @display("p=212,118;is=n;i=device/server");
        }
        router: Router {
            @display("p=351.81198,115.771996;i=device/smallrouter");
        }
        upf: Upf {
            @display("p=500,115");
        }
        iUpf: Upf {
            @display("p=500,215");
        }
    """

    t2 = ""
    for i in range(gnbs):
        t2 += f"""
        gnb{i}: gNodeB {{
            @display("p=250,300;is=vl");
        }}
            """

    t3 = """
        bgCell[numBgCells]: BackgroundCell {
            @display("p=249.528,483.31998;is=vl");
        }
        ue[numUe]: NRUe {
            @display("p=499.056,428.244");
        }
    connections:

        //# Data Network connections
        server.pppg++ <--> Eth10G <--> router.pppg++;
        router.pppg++ <--> Eth10G <--> upf.filterGate;

        //# 5G Core Network connections
        upf.pppg++ <--> Eth10G <--> iUpf.pppg++;
            """
    t4 = ""
    for i in range(gnbs):
        t4 += f"""
        iUpf.pppg++ <--> Eth10G <--> gnb{i}.ppp;
    """

    t5 = ""

    l = list(range(gnbs))
    combinations = list(zip(l, l[1:]))
    combinations.append((l[-1], l[0]))
    if gnbs > 1:
        for comb in combinations:

            t5 += f"""
        gnb{comb[0]}.x2++ <--> Eth10G <--> gnb{comb[1]}.x2++;
        """

    t6 = "}"

    ff = t1+t2+t3+t4+t5+t6
    with open("MultiCell_Standalone.ned", "w") as f:
        f.write(ff)



    a1 = """
[General]
image-path=../../images
sim-time-limit=900s
warmup-period=0s
#repeat = 3
**.routingRecorder.enabled = false

############### Statistics ##################
output-scalar-file = ${resultdir}/${configname}/${repetition}.sca
output-vector-file = ${resultdir}/${configname}/${repetition}.vec
seed-set = ${repetition}
**.vector-recording = false

############### General PHY parameters ##############
**.downlink_interference = true
**.uplink_interference = true
**.ueTxPower = 26
**.eNodeBTxPower = 46
**.targetBler = 0.01
**.blerShift = 5  
**.fbPeriod = 40   # reports CQI every 40ms


############### IPv4 configurator config #################
*.configurator.config = xmldoc("./demo.xml")

network = MultiCell_Standalone

############# Floorplan parameters ################
**.mobility.constraintAreaMaxX = 4100m
**.mobility.constraintAreaMaxY = 2950m
**.mobility.constraintAreaMinX = 0m
**.mobility.constraintAreaMinY = 0m
**.mobility.constraintAreaMinZ = 0m
**.mobility.constraintAreaMaxZ = 0m
**.mobility.initFromDisplayString = false

################ SCTP configuration ##################
**.sctp.nagleEnabled = false         # if true, transmission of small packets will be delayed on the X2
**.sctp.enableHeartbeats = false

############### BS position and number #################
    """

    a2 = ""
    for i, c in enumerate(clusters):
        a2 += f"""
*.gnb{i}.mobility.initialX = {clusters[i][0]}m
*.gnb{i}.mobility.initialY = {clusters[i][1]}m
    """
    a3 = f"""
*.gnb*.numX2Apps = {2 if gnbs > 1 else 0}
"""
    a3 += """

############### X2 configuration #################
# one x2App per peering node
*.gnb*.x2App[*].server.localPort = 5000 + ancestorIndex(1)  # Server ports (x2App[0]=5000, x2App[1]=5001, ...)
    """
    a4 = ""
    if gnbs > 1:
        for comb in combinations:
            a4 += f"""
*.gnb{comb[0]}.x2App[1].client.connectAddress = "gnb{comb[1]}%x2ppp0"
*.gnb{comb[1]}.x2App[0].client.connectAddress = "gnb{comb[0]}%x2ppp0"
            """


    a5 = """

############## UE configuration ##################
*.numUe = ${numUEs=18}

# UEs associates to the best BS at the beginning of the simulation
*.ue[*].macCellId = 0
*.ue[*].masterId = 0
*.ue[*].nrMacCellId = 1
*.ue[*].nrMasterId = 1
**.dynamicCellAssociation = true
**.enableHandover = true

############ UEs position #################
*.ue[*].mobility.typename = "StationaryMobility" #all UEs are in the specified fixed position
#*.ue[*].mobility.speed = 0mps 
#------------------------------------#

#ues positions 

*.ue[0].mobility.initialX = 10m
*.ue[0].mobility.initialY =  2950m - 1697.5m

*.ue[1].mobility.initialX = 10m
*.ue[1].mobility.initialY =  2950m - 947.5m

*.ue[2].mobility.initialX = 10m
*.ue[2].mobility.initialY =  2950m - 10m

*.ue[3].mobility.initialX = 1447.5m
*.ue[3].mobility.initialY =  2950m - 10m

*.ue[4].mobility.initialX = 3260m
*.ue[4].mobility.initialY =  2950m - 10m

*.ue[5].mobility.initialX = 4010m
*.ue[5].mobility.initialY =  2950m - 10m

*.ue[6].mobility.initialX = 4010m
*.ue[6].mobility.initialY =  2950m - 385m

*.ue[7].mobility.initialX = 4010m
*.ue[7].mobility.initialY =  2950m - 822.5m

*.ue[8].mobility.initialX = 4010m
*.ue[8].mobility.initialY =  2950m - 1385m

*.ue[9].mobility.initialX = 4010m
*.ue[9].mobility.initialY =  2950m - 1947.5m

*.ue[10].mobility.initialX = 4010m
*.ue[10].mobility.initialY =  2950m - 2385m

*.ue[11].mobility.initialX = 4010m
*.ue[11].mobility.initialY =  2950m - 2885m

*.ue[12].mobility.initialX = 3260m
*.ue[12].mobility.initialY =  2950m - 510m

*.ue[13].mobility.initialX = 2822.5m
*.ue[13].mobility.initialY =  2950m - 510m

*.ue[14].mobility.initialX = 2322.5m
*.ue[14].mobility.initialY =  2950m - 510m

*.ue[15].mobility.initialX = 3260m
*.ue[15].mobility.initialY =  2950m - 760m

*.ue[16].mobility.initialX = 3260m
*.ue[16].mobility.initialY =  2950m - 1010m

*.ue[17].mobility.initialX = 3260m
*.ue[17].mobility.initialY =  2950m - 1385m

# one UDP application for each user
*.ue[0].numApps = 17
*.ue[1].numApps = 1
*.ue[2].numApps = 1
*.ue[3].numApps = 1
*.ue[4].numApps = 1
*.ue[5].numApps = 1
*.ue[6].numApps = 1
*.ue[7].numApps = 1
*.ue[8].numApps = 1
*.ue[9].numApps = 1
*.ue[10].numApps = 1
*.ue[11].numApps = 1
*.ue[12].numApps = 1
*.ue[13].numApps = 1
*.ue[14].numApps = 1
*.ue[15].numApps = 1
*.ue[16].numApps = 1
*.ue[17].numApps = 1

#*.server.numApps = 18

#*.server.app[*].typename = "VoIPReceiver"
#*.server.app[*].localPort = 3000 + ancestorIndex(0)

#*.ue[*].app[*].typename = "VoIPSender"
#*.ue[*].app[*].PacketSize = 34
#*.ue[*].app[*].sampling_time = 0.02s
#*.ue[*].app[*].destAddress = "server" 
#*.ue[*].app[*].destPort = 3000 + ancestorIndex(0) 
#*.ue[*].app[*].localPort = 3088+ancestorIndex(0)
#*.ue[*].app[*].startTime = 0s

*.ue[*].app[*].finishTime = 3610s

# Transmitter(s)
*.ue[0].app[*].typename = "VoIPReceiver"
*.ue[0].app[*].localPort = 3000 + ancestorIndex(0)
*.ue[0].app[*].startTime = 10s

*.ue[1].app[*].typename = "VoIPSender"
*.ue[1].app[*].PacketSize = 34
*.ue[1].app[*].sampling_time = 0.02s
*.ue[1].app[*].localPort = 3089
*.ue[1].app[*].startTime = 10s
*.ue[1].app[*].destAddress = "ue[0]"
*.ue[1].app[*].destPort = 3001

*.ue[2].app[*].typename = "VoIPSender"
*.ue[2].app[*].PacketSize = 34
*.ue[2].app[*].sampling_time = 0.02s
*.ue[2].app[*].localPort = 3090
*.ue[2].app[*].startTime = 10s
*.ue[2].app[*].destAddress = "ue[0]"
*.ue[2].app[*].destPort = 3002

*.ue[3].app[*].typename = "VoIPSender"
*.ue[3].app[*].PacketSize = 34
*.ue[3].app[*].sampling_time = 0.02s
*.ue[3].app[*].localPort = 3091
*.ue[3].app[*].startTime = 10s
*.ue[3].app[*].destAddress = "ue[0]"
*.ue[3].app[*].destPort = 3003 

*.ue[4].app[*].typename = "VoIPSender"
*.ue[4].app[*].PacketSize = 34
*.ue[4].app[*].sampling_time = 0.02s
*.ue[4].app[*].localPort = 3092
*.ue[4].app[*].startTime = 10s
*.ue[4].app[*].destAddress = "ue[0]"
*.ue[4].app[*].destPort = 3004

*.ue[5].app[*].typename = "VoIPSender"
*.ue[5].app[*].PacketSize = 34
*.ue[5].app[*].sampling_time = 0.02s
*.ue[5].app[*].localPort = 3093
*.ue[5].app[*].startTime = 10s 
*.ue[5].app[*].destAddress = "ue[0]"
*.ue[5].app[*].destPort = 3005 

*.ue[6].app[*].typename = "VoIPSender"
*.ue[6].app[*].PacketSize = 34
*.ue[6].app[*].sampling_time = 0.02s
*.ue[6].app[*].localPort = 3094
*.ue[6].app[*].startTime = 10s
*.ue[6].app[*].destAddress = "ue[0]"
*.ue[6].app[*].destPort = 3006

*.ue[7].app[*].typename = "VoIPSender"
*.ue[7].app[*].PacketSize = 34
*.ue[7].app[*].sampling_time = 0.02s
*.ue[7].app[*].localPort = 3095
*.ue[7].app[*].startTime = 10s
*.ue[7].app[*].destAddress = "ue[0]"
*.ue[7].app[*].destPort = 3007 

*.ue[8].app[*].typename = "VoIPSender"
*.ue[8].app[*].PacketSize = 34
*.ue[8].app[*].sampling_time = 0.02s
*.ue[8].app[*].localPort = 3096
*.ue[8].app[*].startTime = 10s
*.ue[8].app[*].destAddress = "ue[0]"
*.ue[8].app[*].destPort = 3008 

*.ue[9].app[*].typename = "VoIPSender"
*.ue[9].app[*].PacketSize = 34
*.ue[9].app[*].sampling_time = 0.02s
*.ue[9].app[*].localPort = 3097
*.ue[9].app[*].startTime = 10s
*.ue[9].app[*].destAddress = "ue[0]"
*.ue[9].app[*].destPort = 3009

*.ue[10].app[*].typename = "VoIPSender"
*.ue[10].app[*].PacketSize = 34
*.ue[10].app[*].sampling_time = 0.02s
*.ue[10].app[*].localPort = 3098
*.ue[10].app[*].startTime = 10s
*.ue[10].app[*].destAddress = "ue[0]"
*.ue[10].app[*].destPort = 3010 

*.ue[11].app[*].typename = "VoIPSender"
*.ue[11].app[*].PacketSize = 34
*.ue[11].app[*].sampling_time = 0.02s
*.ue[11].app[*].localPort = 3099
*.ue[11].app[*].startTime = 10s
*.ue[11].app[*].destAddress = "ue[0]"
*.ue[11].app[*].destPort = 3011 

*.ue[12].app[*].typename = "VoIPSender"
*.ue[12].app[*].PacketSize = 34
*.ue[12].app[*].sampling_time = 0.02s
*.ue[12].app[*].localPort = 3100
*.ue[12].app[*].startTime = 10s
*.ue[12].app[*].destAddress = "ue[0]"
*.ue[12].app[*].destPort = 3012 

*.ue[13].app[*].typename = "VoIPSender"
*.ue[13].app[*].PacketSize = 34
*.ue[13].app[*].sampling_time = 0.02s
*.ue[13].app[*].localPort = 3101
*.ue[13].app[*].startTime = 10s
*.ue[13].app[*].destAddress = "ue[0]"
*.ue[13].app[*].destPort = 3013

*.ue[14].app[*].typename = "VoIPSender"
*.ue[14].app[*].PacketSize = 34
*.ue[14].app[*].sampling_time = 0.02s
*.ue[14].app[*].localPort = 3102 
*.ue[14].app[*].startTime = 10s
*.ue[14].app[*].destAddress = "ue[0]"
*.ue[14].app[*].destPort = 3014

*.ue[15].app[*].typename = "VoIPSender"
*.ue[15].app[*].PacketSize = 34
*.ue[15].app[*].sampling_time = 0.02s
*.ue[15].app[*].localPort = 3103
*.ue[15].app[*].startTime = 10s
*.ue[15].app[*].destAddress = "server"
*.ue[15].app[*].destPort = 3015

*.ue[16].app[*].typename = "VoIPSender"
*.ue[16].app[*].PacketSize = 34
*.ue[16].app[*].sampling_time = 0.02s
*.ue[16].app[*].localPort = 3104
*.ue[16].app[*].startTime = 10s
*.ue[16].app[*].destAddress = "ue[0]"
*.ue[16].app[*].destPort = 3016

*.ue[17].app[*].typename = "VoIPSender"
*.ue[17].app[*].PacketSize = 34
*.ue[17].app[*].sampling_time = 0.02s
*.ue[17].app[*].localPort = 3105
*.ue[17].app[*].startTime = 10s
*.ue[17].app[*].destAddress = "ue[0]"
*.ue[17].app[*].destPort = 3000
    """

    fin = a1+a2+a3+a4+a5
    with open("voipGnb.ini", "w") as f:
        f.write(fin)


    


    for file in glob.glob("*.txt"):
        os.remove(file)

    shutil.rmtree('results', ignore_errors=True)
    random.seed(10)


    print("voipGnb.ini created")
    command = './PMUSimulations -r 0 -m -u Cmdenv -n .:../../inet4.4/examples:../../inet4.4/showcases:../../inet4.4/src:../../inet4.4/tests/networks:../../inet4.4/tests/validation:../../inet4.4/tutorials:../../simu5g/emulation:../../simu5g/simulations:../../simu5g/src -x "inet.common.selfdoc;inet.showcases.visualizer.osg;inet.showcases.emulation;inet.transportlayer.tcp_lwip;inet.applications.voipstream;inet.visualizer.osg;inet.examples.voipstream;simu5g.simulations.LTE.cars;simu5g.simulations.NR.cars;simu5g.nodes.cars" --image-path=../../inet4.4/images:../../simu5g/images -l ../../inet4.4/src/INET -l ../../simu5g/src/simu5g voipGnb.ini'

    import subprocess
    subprocess.call(command, shell=True)

    # make a dir
    time.sleep(5)
    import os
    sim_folder = f"sim{gnbs}_gnbs"
    os.mkdir(sim_folder)

    # move all the txt files to the dir

    for file in glob.glob("*.txt"):
        shutil.move(file, sim_folder)

    shutil.copytree('results', os.path.join(sim_folder, 'results'), dirs_exist_ok=True)

    # copy the ini file to the dir

    shutil.copy("voipGnb.ini", sim_folder)
    shutil.copy("MultiCell_Standalone.ned", os.path.join(sim_folder, 'MultiCell_Standalone.conf'))
