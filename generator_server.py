def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)

import random
import glob
import shutil
import os
from itertools import cycle

# delete all txt files in the folder

for file in glob.glob("*.txt"):
    os.remove(file)

random.seed(10)
for mult in range(1, 18): 
    gnb_pos = [(500,500),(500,1200),(500,1900),(500,2600),(1500,2600),(2500,2600),(3500,1200),(3500,1900),(3500,2600)]
    a = range(len(gnb_pos)*mult)

    gg = list(zip(a, cycle(gnb_pos)))


    numUEs = len(gg)
    a = f"*.numUe = ${{numUEs={numUEs}}}"

    general = """
[General]
image-path=../../images
sim-time-limit=900s
warmup-period=0s
#repeat = 3
**.routingRecorder.enabled = false

############### Statistics ##################
output-scalar-file = ${resultdir}/${configname}/${repetition}.sca
output-vector-file = ${resultdir}/${configname}/${repetition}.vec
seed-set = ${repetition}
**.vector-recording = false

############### General PHY parameters ##############
**.downlink_interference = true
**.uplink_interference = true
**.ueTxPower = 26
**.eNodeBTxPower = 46
**.targetBler = 0.01
**.blerShift = 5  
**.fbPeriod = 40   # reports CQI every 40ms


############### IPv4 configurator config #################
*.configurator.config = xmldoc("./demo.xml")

network = MultiCell_Standalone

############# Floorplan parameters ################
**.mobility.constraintAreaMaxX = 4100m
**.mobility.constraintAreaMaxY = 2950m
**.mobility.constraintAreaMinX = 0m
**.mobility.constraintAreaMinY = 0m
**.mobility.constraintAreaMinZ = 0m
**.mobility.constraintAreaMaxZ = 0m
**.mobility.initFromDisplayString = false

############### CA configuration ################# 
#*.carrierAggregation.numComponentCarriers = 2
#*.carrierAggregation.componentCarrier[0].carrierFrequency = 30GHz
#*.carrierAggregation.componentCarrier[0].numBands = 50
#*.carrierAggregation.componentCarrier[1].carrierFrequency = 300GHz
#*.carrierAggregation.componentCarrier[1].numBands = 50
#
#*.gnb.cellularNic.numCarriers = 2
#*.gnb.cellularNic.channelModel[0].componentCarrierIndex = 0
#*.gnb.cellularNic.channelModel[1].componentCarrierIndex = 1
#
#*.ue[*].cellularNic.numNRCarriers = 2
#*.ue[*].cellularNic.nrChannelModel[0].componentCarrierIndex = 0
#*.ue[*].cellularNic.nrChannelModel[1].componentCarrierIndex = 1

################ SCTP configuration ##################
**.sctp.nagleEnabled = false         # if true, transmission of small packets will be delayed on the X2
**.sctp.enableHeartbeats = false

############### BS position and number #################

#First column 

*.gnb0.mobility.initialX = 500m
*.gnb0.mobility.initialY = 500m
*.gnb1.mobility.initialX = 500m
*.gnb1.mobility.initialY = 1200m
*.gnb2.mobility.initialX = 500m
*.gnb2.mobility.initialY = 1900m
*.gnb3.mobility.initialX = 500m
*.gnb3.mobility.initialY = 2600m

#Second column 

#*.gnb4.mobility.initialX = 1500m
#*.gnb4.mobility.initialY = 500m
#*.gnb5.mobility.initialX = 1500m
#*.gnb5.mobility.initialY = 1200m
#*.gnb6.mobility.initialX = 1500m
#*.gnb6.mobility.initialY = 1900m
*.gnb4.mobility.initialX = 1500m
*.gnb4.mobility.initialY = 2600m

#Third Column

#*.gnb8.mobility.initialX = 2500m
#*.gnb8.mobility.initialY = 500m
#*.gnb9.mobility.initialX = 2500m
#*.gnb9.mobility.initialY = 1200m
#*.gnb10.mobility.initialX = 2500m
#*.gnb10.mobility.initialY = 1900m
*.gnb5.mobility.initialX = 2500m
*.gnb5.mobility.initialY = 2600m

#Fourth Column

#*.gnb12.mobility.initialX = 3500m
#*.gnb12.mobility.initialY = 0m
#*.gnb13.mobility.initialX = 3500m
#*.gnb13.mobility.initialY = 500m
*.gnb6.mobility.initialX = 3500m
*.gnb6.mobility.initialY = 1200m
*.gnb7.mobility.initialX = 3500m
*.gnb7.mobility.initialY = 1900m
*.gnb8.mobility.initialX = 3500m
*.gnb8.mobility.initialY = 2600m

############### X2 configuration #################
*.gnb*.numX2Apps = 2                                        # one x2App per peering node
*.gnb*.x2App[*].server.localPort = 5000 + ancestorIndex(1)  # Server ports (x2App[0]=5000, x2App[1]=5001, ...)

*.gnb0.x2App[0].client.connectAddress = "gnb1%x2ppp0" 
*.gnb1.x2App[0].client.connectAddress = "gnb0%x2ppp0"

*.gnb2.x2App[0].client.connectAddress = "gnb1%x2ppp1" 
*.gnb1.x2App[1].client.connectAddress = "gnb2%x2ppp0" 

*.gnb3.x2App[0].client.connectAddress = "gnb2%x2ppp1" 
*.gnb2.x2App[1].client.connectAddress = "gnb3%x2ppp0"

*.gnb4.x2App[0].client.connectAddress = "gnb3%x2ppp1" 
*.gnb3.x2App[1].client.connectAddress = "gnb4%x2ppp0"

*.gnb5.x2App[0].client.connectAddress = "gnb4%x2ppp1" 
*.gnb4.x2App[1].client.connectAddress = "gnb5%x2ppp0"

*.gnb6.x2App[0].client.connectAddress = "gnb5%x2ppp1" 
*.gnb5.x2App[1].client.connectAddress = "gnb6%x2ppp0"

*.gnb7.x2App[0].client.connectAddress = "gnb6%x2ppp1" 
*.gnb6.x2App[1].client.connectAddress = "gnb7%x2ppp0"

*.gnb8.x2App[0].client.connectAddress = "gnb7%x2ppp1" 
*.gnb7.x2App[1].client.connectAddress = "gnb8%x2ppp0"

*.gnb8.x2App[1].client.connectAddress = "gnb0%x2ppp1" 
*.gnb0.x2App[1].client.connectAddress = "gnb8%x2ppp1"
    """

    fixed = """
# UEs associates to the best BS at the beginning of the simulation
*.ue[*].macCellId = 0
*.ue[*].masterId = 0
*.ue[*].nrMacCellId = 1
*.ue[*].nrMasterId = 1
**.dynamicCellAssociation = true
**.enableHandover = true

############ UEs position #################
*.ue[*].mobility.typename = "StationaryMobility"
    """

    positions = ""
    for i in gg:

        positions += f"""
*.ue[{i[0]}].mobility.initialX = {clamp(random.randint(i[1][0]-300, i[1][0]+300), 0, 4100)}m
*.ue[{i[0]}].mobility.initialY =  {clamp(random.randint(i[1][1]-300, i[1][1]+300), 0, 2950)}m
    """

    #print(positions)

    apps_ue0 = f"""
*.ue[*].numApps = 1
*.server.numApps = {numUEs}
"""

    fixed2 = """
*.server.app[*].typename = "VoIPReceiver"
*.server.app[*].localPort = 3000 + ancestorIndex(0)
*.ue[*].app[*].finishTime = 3610s
    """
    start_port_local = 4000
    start_port_remote = 3000

    apps = ""
    for i in range(numUEs):
        apps += f"""
*.ue[{i}].app[*].typename = "VoIPSender"
*.ue[{i}].app[*].PacketSize = 34
*.ue[{i}].app[*].sampling_time = 0.02s
*.ue[{i}].app[*].localPort = {start_port_local}
*.ue[{i}].app[*].startTime = 10s
*.ue[{i}].app[*].destAddress = "server"
*.ue[{i}].app[*].destPort = {start_port_remote}
    """

        start_port_local += 1
        start_port_remote += 1


    pars = [general, a, fixed, positions, apps_ue0, fixed2, apps]

    out = "\n".join(pars)

    with open("voipServer.ini", "w") as f:
        f.write(out)


    print("voipServer.ini created")
    command = './PMUSimulations -r 0 -m -u Cmdenv -n .:../../inet4.4/examples:../../inet4.4/showcases:../../inet4.4/src:../../inet4.4/tests/networks:../../inet4.4/tests/validation:../../inet4.4/tutorials:../../simu5g/emulation:../../simu5g/simulations:../../simu5g/src -x "inet.common.selfdoc;inet.showcases.visualizer.osg;inet.showcases.emulation;inet.transportlayer.tcp_lwip;inet.applications.voipstream;inet.visualizer.osg;inet.examples.voipstream;simu5g.simulations.LTE.cars;simu5g.simulations.NR.cars;simu5g.nodes.cars" --image-path=../../inet4.4/images:../../simu5g/images -l ../../inet4.4/src/INET -l ../../simu5g/src/simu5g voipServer.ini'

    import subprocess
    subprocess.call(command, shell=True)


    # make a dir

    import os
    sim_folder = f"sim{mult}_ue{numUEs}_server"
    os.mkdir(sim_folder)

    # move all the txt files to the dir

    
    for file in glob.glob("*.txt"):
        shutil.move(file, sim_folder)

    # copy the ini file to the dir

    shutil.copy("voipServer.ini", sim_folder)
