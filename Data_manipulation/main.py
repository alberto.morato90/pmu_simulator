import numpy as np
from statsmodels.distributions.empirical_distribution import ECDF
import matplotlib
import matplotlib.pyplot as plt
import os

simtime = 3600 #s
period = 0.02 #s
senders = 18

matplotlib.rc('xtick', labelsize=14)
matplotlib.rc('ytick', labelsize=14)

def ecdf(x):
    xs = np.sort(x)
    ys = np.arange(1, len(xs)+1)/float(len(xs))
    return xs, ys

directory = "/home/tommasofedullo/Desktop/PMUPlotsNew/"
directory_d2d = "/home/tommasofedullo/Desktop/PMUPlotsNew/data"

##### Server #####
pack_sent = 0
delays = []

for file in os.listdir(directory):
    if ('.txt' in file) and ('send' in file):
        f = open(os.path.join(directory, file))

        for line in Lines:
            pack_sent = pack_sent + 1

        print("Sent = " + str(pack_sent))

        Lines = f.readlines()

    elif ('txt' in file) and('results' in file):
        f = open(os.path.join(directory, file))
        Lines = f.readlines()

        for line in Lines:
            words = line.split()
            try:
                delays.append(float(words[2]))
            except:
                print('error')



np.savetxt("data.csv", delays, delimiter=",")

print("Expected " + str(simtime/period) + " packets per end device. So total " + str(senders*(simtime/period)))
print("Received " + str(len(delays)) + " packets.")
print("PDR = " + str((len(delays)/float(pack_sent))*100))

cutoff = np.percentile(delays, 99)

count = 0
for delay in delays.copy():
    #print(count)
    if delay >= cutoff:
        delays.remove(delay)
    count = count + 1

print("Received " + str(len(delays)) + " packets.")
print("Mean Delay = " + str(np.mean(delays)))
print("Max Delay = " + str(np.max(delays)))
print("STD Delay = " + str(np.std(delays)))


delays.sort()
ecdf_server = ECDF(delays)
#################################################################
del pack_sent
del delays
del count
##### d2d #####
pack_sent = 0
delays = []

for file in os.listdir(directory_d2d):
    if ('.txt' in file) and ('send' in file):
        f = open(os.path.join(directory_d2d, file))

        for line in Lines:
            pack_sent = pack_sent + 1

        print("Sent = " + str(pack_sent))

        Lines = f.readlines()

    elif ('txt' in file) and('results' in file):
        f = open(os.path.join(directory_d2d, file))
        Lines = f.readlines()

        for line in Lines:
            words = line.split()
            try:
                delays.append(float(words[2]))
            except:
                print('error')



np.savetxt("data.csv", delays, delimiter=",")

print("Expected d2d" + str(simtime/period) + " packets per end device. So total " + str(senders*(simtime/period)))
print("Received d2d" + str(len(delays)) + " packets.")
print("PDR = d2d" + str((len(delays)/float(pack_sent))*100))

cutoff = np.percentile(delays, 99)

count = 0
for delay in delays.copy():
    #print(count)
    if delay >= cutoff:
        delays.remove(delay)
    count = count + 1

print("Received d2d" + str(len(delays)) + " packets.")
print("Max Delay = " + str(np.max(delays)))
print("Mean Delay d2d= " + str(np.mean(delays)))
print("STD Delay d2d= " + str(np.std(delays)))


delays.sort()
ecdf_d2d = ECDF(delays)
#################################################################

plt.plot(ecdf_server.x*1000, ecdf_server.y*100, linewidth=3.0, label='Server')
plt.plot(ecdf_d2d.x*1000, ecdf_d2d.y*100, linewidth=3.0, label = 'UE')
plt.legend()
plt.grid()
plt.xlim([0, 25])
plt.xlabel('Delay(ms)', fontsize=14)
plt.ylabel('CDF (%)', fontsize=14)
plt.savefig('CDF.pdf', orientation='landscape')
plt.show()